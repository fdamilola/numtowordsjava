package com.core;

import java.util.ArrayList;
import java.util.HashMap;

public class NumToWord {
	public static int UPPERCASE = 1;
	public static int LOWERCASE = 2;
	private long number;
	private int stringCase;
	
	public NumToWord(long number){
		this.number = number;
	}
	
	public NumToWord(long number, int stringCase){
		this.number = number;
		this.stringCase = stringCase;
	}
	

	public NumToWord(String number){
		this.number = Long.parseLong(number);
	}
	
	public NumToWord(String number, int stringCase){
		this.number = Long.parseLong(number);
		this.stringCase = stringCase;
	}
	
	@SuppressWarnings("serial")
	private final HashMap<Integer, String> placeHolders = new HashMap<Integer, String>() {
        {
            put(1, " Thousand ");
            put(2, " Million ");
            put(3, " Billion ");
            put(4, " Trillion ");
            put(5, " Quadrillion ");
            put(6, " Quintillion ");
        }
    };
    
    public String convertNumberToWord() {
        StringBuilder stringRep = new StringBuilder();
        
        final int splitSize = 3;
        // Split the number into chunks of 3.
        ArrayList<String> chunks = splitStringIntoChunks(reverseString(String.valueOf(this.number)), splitSize);

        // Treat the chunks in reverse.
        int chunksSize = chunks.size();
        for (int i = (chunksSize - 1); i >= 0; i--) {
            int threeDigitNumber = Integer.parseInt(chunks.get(i), 10);

        	int hundredth = threeDigitNumber / 100;
        	if (hundredth > 0) {
                stringRep.append(getWordRepresentationForNumber(hundredth));
        		stringRep.append(" hundred");
        	}

        	int tenth = threeDigitNumber % 100;
        	// Pad with 'and' as necessary
        	if ((tenth > 0) && (i == 0)) {
        		if (hundredth > 0 || chunksSize > 1) {
        			stringRep.append(" and ");
        		}
        	} else if ((tenth > 0) && (i > 0) && hundredth > 0) {
        		stringRep.append(" and ");
        	}

        	if ((tenth > 10) && (tenth <= 19)) {
                stringRep.append(getWordRepresentationForNumber(tenth));
        		stringRep.append(" ");
        	} else {
        		tenth = tenth / 10;
        		if (tenth > 0) {
                    stringRep.append(getWordRepresentationForNumber(tenth * 10));
        			//stringRep.append(' ');
        		}
        		int unit = (threeDigitNumber % 100) % 10;

                if (unit > 0 && tenth > 0) {
                    stringRep.append('-');
                }

        		if (unit > 0) {
                    //System.out.println(unit);
        			stringRep.append(getWordRepresentationForNumber(unit));
        			//stringRep.append(" ");
        		}
        	}
        	if (threeDigitNumber > 0){
        		stringRep.append((placeHolders.get(i) == null ? "" : (placeHolders.get(i))));
        	}
        }
        String finalString = null;
        if (this.stringCase == 1){
        	finalString = stringRep.toString().trim().toUpperCase()+".";
        }else {
        	finalString = stringRep.toString().trim()+".";
        }
        
        return finalString;
    }

    private ArrayList<String> splitStringIntoChunks(String originalString, int chunkSize) {
        int originalStringLength = originalString.length();
        ArrayList<String> chunks = new ArrayList<>();
        for (int i = 0; i < originalStringLength; i = i + chunkSize) {
            // Reverse the pieces before adding it to the collection so the 
            // numbers are in the right order.
            //System.out.println(i);
            chunks.add(reverseString(originalString.substring(
                    i, Math.min(originalStringLength, (i + chunkSize)))));
        }
        //System.out.println("splitstringintochunks() : " + chunks);

        return chunks;
    }

    private String reverseString(String originalString) {
    	String rev = new StringBuilder(originalString).reverse().toString();
    	//System.out.println("reverse string : " + rev);
        return rev;
    }

    
    private String getWordRepresentationForNumber(int number){
    	HashMap<Integer, String> numRep = new HashMap<Integer, String>();
    	
    	numRep.put(1, "One");
    	numRep.put(2, "Two");
    	numRep.put(3, "Three");
    	numRep.put(4, "Four");
    	numRep.put(5, "Five");
    	numRep.put(6, "Six");
    	numRep.put(7, "Seven");
    	numRep.put(8, "Eight");
    	numRep.put(9, "Nine");
    	numRep.put(10, "Ten");
    	numRep.put(11, "Eleven");
    	numRep.put(12, "Twelve");
    	numRep.put(13, "Thirteen");
    	numRep.put(14, "Fourteen");
    	numRep.put(15, "Fifteen");
    	numRep.put(16, "Sixteen");
    	numRep.put(17, "Seventeen");
    	numRep.put(18, "Eighteen");
    	numRep.put(19, "Nineteen");
    	numRep.put(20, "Twenty");
    	numRep.put(30, "Thirty");
    	numRep.put(40, "Fourty");
    	numRep.put(50, "Fifty");
    	numRep.put(60, "Sixty");
    	numRep.put(70, "Seventy");
    	numRep.put(80, "Eighty");
    	numRep.put(90, "Ninety");
    	
    	return numRep.get(number);
    }
}
